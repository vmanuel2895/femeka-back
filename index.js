const express = require('express');
const cors = require('cors');
const fileUpload = require('express-fileupload');
const {dbConnection} = require('./db/config');
// crear servidor
const app = express();
app.use(cors());
//lectura y parseo del body
app.use(express.json());
// app.use(fileUpload());
// **********************************  rutas  ************************************

//ruta nuevo presidente

// app.use( require('./routes/presidentes/presi'));

app.use(require('./routes/indexrutas'));




//******************************************************************************** */
//db connection
dbConnection();
app.listen(3000, () => {
    console.log("servidor backend corriendo puerto 3000");
});