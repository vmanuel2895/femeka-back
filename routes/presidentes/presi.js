const {Router, response} = require('express');
const presidente = require('../../models/presidentes/presidentes');
const RespServer = require('../../fuctions/funciones');
const route = Router();
const pass = require('bcryptjs');


route.get('/obtener/presidentes', async (req,res)=>{

     const respuesta = await presidente.find();
    res.json({
            ok:true,
             respuesta
            // mns:"primer consulta"
        });
})

route.post('/presidente/nuevo',(req,res)=>{

    let body = req.body;

     let presidentenew = new presidente({
      nombre : body.nombre,
  password : body.password,
  estado: body.estado,
  role: body.role
 });//fin new presidente
    

 /// encriptar contaseña
  const salt = pass.genSaltSync();
  presidentenew.password = pass.hashSync(body.password,salt);
 /////////
presidentenew.save();

    res.json({
            ok:true,
            presidentenew
        })
})

route.put('/actualizar/usuario/presidente/:id', async (req,res = response )=>{
    const uid = req.params.id;
try {

    const usuarioDB = presidente.findById(uid);

    if(!usuarioDB){
       res.status(404).json({
       ok: false,
       mns:"No existe ningun usuario con ese ID"
       });
    }

    const campos = req.body;
    delete campos.estado,
    delete campos.role
    const salt = pass.genSaltSync();
   campos.password = pass.hashSync(campos.password,salt);
    const updateUser = await presidente.findByIdAndUpdate(uid, campos, { new:true});

    res.json({
        ok:true,
     updateUser
    });


} catch (error) {
    console.log(error);
    res.status(500).json({
      ok:false,
      mns:"Error al actualizar usuario/presidente"
    });
}
});

module.exports = route;