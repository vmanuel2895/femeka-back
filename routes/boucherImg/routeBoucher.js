const {Router} = require('express');
const imgBou = require('../../models/bouchers/boucher');
const fileUpload = require('express-fileupload');
const {v4: uuidv4 } = require('uuid');
const app = Router();

app.use(fileUpload());
const multer = require('../../storage/boucherImg');

app.post('/agregar/boucher', multer.array('bouchers', 12), (req, res, next) => {
    const body = req.body;
    const imagenesTiket = [];
  
    req.files.forEach(img => {
        imagenesTiket.push(img.originalname);
    });

    const newReturnTiket = new imgBou({
        
        bouchers: imagenesTiket
       
    });

    newReturnTiket.save();
    res.json({
    ok:true,
      mns:"Boucher Guardados"
    });
        // .then(data => serveResp(data, null, 'Se envió nuevo regreso xray', resp))
        // .catch(error => serveResp(null, error, 'No se encontró regreso xray', resp));

});



app.post('/subir/archivo/:tipo',(req,res)=>{

    const tipo = req.params.tipo;
const carpetaImg = ['img'];

if(!carpetaImg.includes(tipo)){
    res.json({
        ok:false,
          mns:"carpeta no asignada"
        });
}
//validar archvio
if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).json({
        ok:false,
        msj:"no hay archivo"
    });
  }

  //procesar imagen
  const file = req.files.bouchers;
  const nombreCortado = file.name.split('.');
  const extencionArchivo = nombreCortado[nombreCortado.length -1];

  //extenciones validas

  const extencionValida = ['png','jpg','gif'];
  if(!extencionValida.includes(extencionArchivo)){
    res.json({
        ok:true,
          mns:"No es una extencion existente"
        });
  }

  //Generar nombre del archivo
  const nomArchivo = `${ uuidv4()}.${extencionArchivo}`; 

  // path guardar imagen
const path =`./Bouchers/${tipo}/${ nomArchivo}`;

// mover imagen
file.mv(path,(err)=>{
    if(err){
        console.log(err);
        return res.status(500).json({
           ok: false,
           msg:"Error al mover imagen"
        });
    }

    res.json({
        ok:true,
        mns:"Arvhico subido",
          nomArchivo
        });
})

});


module.exports = app;