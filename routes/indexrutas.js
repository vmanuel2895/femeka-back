const express = require('express');
const app = express();

app.use(require('./presidentes/presi'));

//####################### Pagina Femeka Isis #############################
app.use(require('./atletas/registroAtletas'));
app.use(require('./entrenadores/routeEntrenadores'));
app.use(require('./jueces/routeJuez'));
app.use(require('./examinador/routeExaminador'));
app.use(require('./login'));
app.use(require('./boucherImg/routeBoucher'));
//########################################################################
module.exports = app;