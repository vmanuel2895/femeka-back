const {Router, response} = require('express');
const Usuario = require('../models/presidentes/presidentes');
const encriptado = require('bcryptjs');
const app = Router();

app.post('/login', async(req,res = response)=>{

    const {nombre, password} = req.body;
try {

   const entrar = await Usuario.findOne({nombre});

   if(!entrar){

   res.status(404).json({
     ok:false,
     msg:"Nombre de usuario no valido"
     });

   }

   //verificar password
   const valiPass = encriptado.compareSync(password, entrar.password);
   if(!valiPass){

    res.status(404).json({
      ok:false,
      msg:"Usuario o Contraseña no son correctos"
      });
 
    }

    // Generar Token
    res.json({
        ok:true,
        mns:"Entramos al login otra vez :D"
    });
} catch (error) {
    console.log(error," error con login");
    res.status(500).json({
        ok:false,
        mns:"EROR AL LOGEAR"
    });
}

});

module.exports = app;